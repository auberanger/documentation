const path = require('path');
const webpack = require('webpack');
const ManifestPlugin = require('webpack-manifest-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const plugins = [
  new webpack.EnvironmentPlugin({
    NODE_ENV: 'development', // use 'development' unless process.env.NODE_ENV is defined
  }),
  new ManifestPlugin({
    fileName: '../../_data/webpack.json',
    basePath: '/js/build/',
  }),
  new webpack.ProvidePlugin({
    jQuery: 'jquery/dist/jquery.slim.js',
    $: 'jquery/dist/jquery.slim.js',
    Collapse: 'exports-loader?Collapse!bootstrap/js/dist/collapse',
    Dropdown: 'exports-loader?Dropdown!bootstrap/js/dist/dropdown',
    Tab: 'exports-loader?Tab!bootstrap/js/dist/tab',
  }),
  new webpack.LoaderOptionsPlugin({
    minimize: true,
  }),
  new webpack.NamedModulesPlugin(),
];

if (process.env.NODE_ENV === 'debug') {
  plugins.push(new BundleAnalyzerPlugin());
}

module.exports = {
  devtool:
    process.env.NODE_ENV === 'production' ? 'source-map' : 'cheap-source-map',

  entry: {
    common: './js/src/common.js',
    //documentation: './documentation.js',
    install: './js/src/install.js',
    //nightly: './nightly.js',
    //packages: './packages.js',
    //package: './package.js',
    api: './js/src/api.js',
  },
  output: {
    path: path.resolve(__dirname, 'js/build'),
    publicPath: '/js/build/',
    filename:
      process.env.NODE_ENV === 'production'
        ? '[name].[chunkhash].js'
        : '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
  resolve: {
    alias: {
      jquery: 'jquery/dist/jquery.slim.js',
    },
  },
  plugins,
  stats: 'minimal',
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  optimization: {
    runtimeChunk: 'single', // enable "runtime" chunk
    splitChunks: {
      cacheGroups: {
        vendor: {
          //test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all',
          enforce: true,
          priority: 1,
          test(module, chunks) {
            const name = module.nameForCondition && module.nameForCondition();
            return chunks.some(
              chunk => chunk.name !== 'api' && /node_modules/.test(name)
            );
          },
        },
      },
    },
  },
};
