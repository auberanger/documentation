import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, withRouter } from 'react-router-dom';
import { RedocStandalone } from 'redoc';
import Select from 'react-select';
import * as qs from 'query-string';
import * as optionsAdditions from './releases.json';

const options = [{ value: 'develop', label: 'develop branch' }].concat(
  optionsAdditions
);

class SelectVersion extends React.Component {
  version = qs.parse(this.props.location.search).version || 'develop';
  handleChange = (ValueType, ActionTypes) => {
    // setting the version from the dropdown
    this.version = ValueType.value;
    this.setState(ValueType);

    // propagating the change to the url parameters
    this.props.history.push({
      pathname: this.props.history.location.pathname,
      search: '?version=' + ValueType.value,
    });

    window.location.reload();
  };

  render() {
    const customStyles = {
      option: (provided, state) => ({
        ...provided,
        color: state.isSelected ? '#f48a44' : 'black',
        menuPortal: base => ({ ...base, zIndex: 9999 }),
      }),
    };

    return (
      <Select
        defaultValue={options.find(e => e.value == this.version)}
        options={options}
        theme={theme => ({
          ...theme,
          colors: {
            ...theme.colors,
            text: 'white',
            primary25: '#f48a44',
            primary: 'black',
          },
        })}
        menuPortalTarget={document.body}
        styles={customStyles}
        onChange={this.handleChange}
      />
    );
  }
}

const SelectVersionLocation = withRouter(SelectVersion);

class Documentation extends React.Component {
  version = qs.parse(this.props.location.search).version || 'develop';
  url =
    this.props.dataSwitch == 'peertube'
      ? 'https://raw.githubusercontent.com/Chocobozzz/PeerTube/' +
        this.version +
        '/support/doc/api/openapi.yaml'
      : '/assets/instances-openapi.yaml';

  componentDidMount() {
    this.props.history.listen((location, action) => {
      console.log(action, location.pathname, location.search);
    });
  }

  render() {
    return (
      <RedocStandalone
        specUrl={this.url}
        options={{
          nativeScrollbars: true,
          requiredPropsFirst: true,
          sortPropsAlphabetically: true,
          theme: {
            colors: {
              primary: { main: '#f48a44' },
            },
          },
        }}
      />
    );
  }
}

const DocumentationLocation = withRouter(Documentation);

const apiElement = document.getElementById('api');
ReactDOM.render(
  <BrowserRouter>
    <DocumentationLocation
      dataSwitch={apiElement.getAttribute('data-switch')}
    />
  </BrowserRouter>,
  apiElement
);

if (apiElement.getAttribute('data-switch') == 'peertube') {
  ReactDOM.render(
    <BrowserRouter>
      <SelectVersionLocation />
    </BrowserRouter>,
    document.getElementById('selectVersion')
  );
}
