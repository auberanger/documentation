---
id: docs_usage
guide: docs_getting_started
layout: guide
timer: false
additional_reading_tags: ['basics']
---

{% include vars.html %}

Now that you have PeerTube [installed]({{url_base}}/docs/install.html), you should expose it to the world! For that you need a reverse-proxy like Nginx (spoilers, that's the one we fully support!). You want to configure it now.

- [Nginx configuration](https://github.com/Chocobozzz/PeerTube/blob/develop/support/nginx/peertube) (officially supported)
- [Apache2/httpd configuration](https://gist.github.com/rigelk/07a0b8963fa4fc1ad756374c28479bc7) (community supported)
