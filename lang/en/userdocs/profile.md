---
id: userdocs_profile
guide: userdocs_profile
layout: userguide
additional_reading_tags: ["video", "channel", "sharing"]
---

## Create an account on PeerTube <a class="toc" id="toc-create-an-account-on-peertube" href="#toc-create-an-account-on-peertube"></a>

To be able to send a video, you must have an account on an _instance_ (a server running PeerTube). You can see all instances on [instances.joinpeertube.org](https://instances.joinpeertube.org/instances).

Once you found an instance suiting you, you only need click the button "Create an account" and fill in you **login** (nickname), a **mail address** and a **password**.

![Registration page after clicking on "Create an account"](/assets/profile_registration.png){:class="img-fluid"}

## Connect to your PeerTube instance <a class="toc" id="toc-connect-to-your-peertube-instance" href="#toc-connect-to-your-peertube-instance"></a>

To connect, you have to go the address of the instance where you registered ; indeed, even if instances show each other's videos, the index of accounts is not federated.
Click in the top left-hand corner, on the "Login" button then fill in your login or mail address and your password.

**Beware** to respect the word case in both fields.

Once connected, your nickname and mail address appear below the instance name. Congrats, you are now connected!

## Update your profile <a class="toc" id="toc-update-your-profile" href="#toc-update-your-profile"></a>

To update your profile, change your avatar, your password, etc. You only need to click on the 3 vertical dots near your profile name, then "My Settings". You then have a few options:

1.  My settings
1.  My library
1.  Ownership changes

![Account library management and settings](/assets/profile_library.png){:class="img-fluid"}

In the tab **_My Settings_**, you can:

* change your avatar by sending a .png, .jpeg, .jpg, with a maximum size of 100KB from your computer.
* see your quota used. This quota is given by the instance hosting your account, and can vary from a few MB to thousands of GB, or even be unlimited. On instances that create versions of different quality of your videos after uploading them (after a "transcoding" step of the video), the disk space occupied by these videos is the one taken into account, not the one of the video you sent.
* modify your display name, which is different from the login you use and which cannot be modified.
* add a description of your profile, which will be displayed on your public profile. It is often the first thing seen by visitors and users of PeerTube, and represents your identity, so don't neglect it.
* change your password, by typing twice your password and clicking **_Change Password_**.
* for some instances containing sensitive videos, you can choose how to display them: with, without blur or even not at all.
* finally, you can choose to play videos automatically or not with a checkbox.
