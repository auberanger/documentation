---
id: userdocs_getting_started
guide: userdocs_getting_started
layout: userguide
---

<div class="alert alert-info" role="alert">
  This guide assumes you already have chosen an instance via <a href="http://joinpeertube.org/">joinpeertube.org</a> - or any other way - and provides you with further details on how to make PeerTube a place to call home! But first, a little recap' of what PeerTube is.
</div>

## What is PeerTube? <a class="toc" id="toc-what-is-peertube" href="#toc-what-is-peertube"></a>

PeerTube is a video hosting software. It allows you to watch and share videos
with others using your own server, be it a simple computer board at home or a blade in a datacenter. You don't need to host thousands of videos to make your instance interesting for daily use. In a federated fashion, it will talk _with other servers of your choice_ to aggregate references to their videos without really hosting them locally. What's more, we also aim to play nice with others: venerable MediaGobelin instances that have filled the gap of video hosting for so many years will eventually get federated with PeerTube instances, making the whole federation even bigger.

PeerTube also relies on existing open technologies like BitTorrent as a streaming transport layer, making people watching your video able to share it while they watch it (and even beyond that provided they have a WebTorrent-compatible torrent daemon). This has the advantage of lowering the load on the server, making it _slightly_ more scalable.

PeerTube is close to other federated communities, traditionally more oriented towards purely social interaction in a Twitter-like fashion. We also interact with them thanks to ActivityPub, rendering videos commentable all over the fediverse! Plus, you can follow them thanks to ActivityStreams and RSS/Atom/Json-feed feeds.

Last but not least, PeerTube is an open platform that manages to aggregate these wonderful technologies. There are many more to come, but the most wonderful is what you do with it: sharing video, music, finally unleashed.

## Why do we need it? <a class="toc" id="toc-why-do-we-need-it" href="#toc-why-do-we-need-it"></a>

Plenty of reasons. Ask yourself why you are here.

It could be because you want to find back the sense of community-driven content creation. It's not about creating closed communities, but rather about knowing the content comes in a natural way and not driven by an ominous and obscure algorithm, nor by advertisers that drive content based on how much they can make you buy with it.

It could be because you got riped off elsewhere (getting bullied by ContentID and losing days of revenue).

It could be because you want to use open source, libre technologies (we're not especially fond of GNU philosophy, but that strikes a cord as opposed to regular open source).

It could be because you want to use the best formats available and make them spread easily. We're actively working on this.

It could be because you don't want to be tracked.

## How ? <a class="toc" id="toc-how" href="#toc-how"></a>

Because we could. And now it's your turn.
