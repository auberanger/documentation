---
id: devdocs_troubleshooting
layout: guide
---

#### ENOSPC: no space left on device <a class="toc" id="toc-enospc" href="#toc-enospc"></a>

Depending on your OS, you may face the following error :

```sh
# [nodemon] Internal watch failed: ENOSPC: no space left on device, watch '/PeerTube/dist'
```

This is due to your system's limit on the number of files you can monitor for live-checking changes. For example, Ubuntu uses inotify and this limit is set to 8192. Then you need to change this limit :

```sh
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```

See more information [here](https://github.com/guard/listen/wiki/Increasing-the-amount-of-inotify-watchers).
